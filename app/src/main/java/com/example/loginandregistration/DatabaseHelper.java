package com.example.loginandregistration;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper{
    public static final String dbname = "Registration";
    public static final String tblname ="User";
    public static final String col_1 = "id";
    public static final String col_2 = "email";
    public static final String col_3 = "password";
    public static final String col_4 = "cpassword";


    public DatabaseHelper(Context context) {
        super(context, dbname, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table User (id integer primary key autoincrement , email text , password text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists "+ tblname);
        onCreate(sqLiteDatabase);


    }

    public boolean insertData (String email,String password ){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(col_2,email);
        contentValues.put(col_3,password);

        long result =sqLiteDatabase.insert(tblname,null,contentValues);

        if (result == -1)
            return false;
        else
            return true;

    }

    public boolean mailexist(String email){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cur = sqLiteDatabase.rawQuery("select * from  User where email = ?",new  String[]{email});
        if (cur.getCount()>0)
            return true;
        else
            return false;
    }
}
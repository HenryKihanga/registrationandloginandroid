package com.example.loginandregistration;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DatabaseHelper myDb;
    EditText editTextEmail,editTextPassword,editTextCpassword;
    Button buttonSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDb = new DatabaseHelper(this);

        editTextEmail = findViewById(R.id.editText_email);
        editTextPassword= findViewById(R.id.editText_pass);
        editTextCpassword = findViewById(R.id.editText_cpass);
        buttonSignin = findViewById(R.id.button_Register);

        signUp();
    }

    public void signUp(){
        buttonSignin.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String s1 = editTextEmail.getText().toString();
                        String s2 = editTextPassword.getText().toString();
                        String s3 = editTextCpassword.getText().toString();

                        if (s1.equals("")|| s2.equals("")||s3.equals("")){
                            Toast.makeText(MainActivity.this,"Fill all fields",Toast.LENGTH_LONG).show();
                        }

                        else{
                            if (s2.equals(s3)){
                                boolean ismailexist = myDb.mailexist(s1);
                                if (ismailexist==false){
                                    boolean insert = myDb.insertData(s1,s2);
                                    if (insert==true)
                                        Toast.makeText(MainActivity.this,"Registered Successful",Toast.LENGTH_LONG).show();
                                    else
                                        Toast.makeText(MainActivity.this,"Registration Incomplete",Toast.LENGTH_LONG).show();

                                }
                                else
                                    Toast.makeText(MainActivity.this,"Email already exist",Toast.LENGTH_LONG).show();
                            }

                            else
                                Toast.makeText(MainActivity.this,"Password Mismatch",Toast.LENGTH_LONG).show();


                        }

                    }
                }
        );
    }
}
